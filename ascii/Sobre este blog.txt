                           _________________

                            SOBRE ESTE BLOG

                              HydraDarkNg
                           _________________


Table of Contents
_________________

Sobre el montaje
Como exporto este blog
El tema





Sobre el montaje
================

  Esta montado en un proyecto gitlab, específicamente aquí
  <https://gitlab.com/HydraDarkNg-blog/HydraDarkNg-blog.gitlab.io>.

  El tutorial que seguí fue el de un blog llamado el [elblogdelazaro]
  específicamente este [post].


[elblogdelazaro] <https://elblogdelazaro.org/>

[post]
<https://elblogdelazaro.org/posts/2018-12-10-sitio-web-estatico-gitlab/>


Como exporto este blog
======================

  El blog esta hecho en Emacs con `org-publish', [aquí] esta en su
  versión en html ya que es un documento de `org'.


[aquí] <../articles/build.html>


El tema
=======

  Mi tema de html esta hecho para que se parezca lo más posible a mi
  Emacs en org mode ya que me gusta el como se ve emacs.



					     Emacs 28.1 (Org mode 9.5.4)